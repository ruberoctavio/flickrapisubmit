$(document).ready(function() {

 $('form').submit(function (event) {
   event.preventDefault(); //prevent the browser of loading a new page
    var $searchField = $('#search');
    var $submitButton = $('#submit');
    
    $searchField.prop("disabled",true); //Bloquea el field
    $submitButton.attr("disabled",true).val("searching..."); //Muestra mensaje de búsqueda y bloquea el botón mientras AJAX funciona
   
    // the AJAX part
    var flickerAPI = "http://api.flickr.com/services/feeds/photos_public.gne?jsoncallback=?";
    var animal = $(this).val();
    var flickrOptions = {
      tags: animal,
      format: "json"
    };
    function displayPhotos(data) {
      var photoHTML = '<ul>';
      $.each(data.items,function(i,photo) {
        photoHTML += '<li class="grid-25 tablet-grid-50">';
        photoHTML += '<a href="' + photo.link + '" class="image">';
        photoHTML += '<img src="' + photo.media.m + '"></a></li>';
      }); // end each
      photoHTML += '</ul>';
      $('#photos').html(photoHTML);
      $searchField.prop("disabled",false);
      $submitButton.attr("disabled",false).val("Search");
    }
    $.getJSON(flickerAPI, flickrOptions, displayPhotos);

  }); //End of submit form

}); // end ready